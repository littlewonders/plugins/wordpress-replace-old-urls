<?php
/*
Plugin Name: [LW] Replace Old URLs
Description: Internal package to replace URLs in output. Can be used for example when switching site domains but where data is still stored in the database with an incorrect domain.
Version: 1.0.0
Author: Jay Williams
Author Email: admin@jaywilliams.me
License: GPLv2 or later

*/

if (defined('URLS_TO_REPLACE')) {
  function callback_replace_old_urls($buffer) {
    foreach (URLS_TO_REPLACE as $url) {
        $buffer = str_replace($url, home_url('/'), $buffer);
    }

    return $buffer;
  }

  function buffer_start_replace_old_urls() { ob_start('callback_replace_old_urls'); }
  function buffer_end_replace_old_urls() { if (ob_get_length()) @ob_end_flush(); }

  add_action('registered_taxonomy', 'buffer_start_replace_old_urls');
  add_action('shutdown', 'buffer_end_replace_old_urls');
}