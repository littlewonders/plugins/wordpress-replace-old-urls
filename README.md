# Wordpress Replace Old URLs

Internal package to replace URLs in output. Can be used for example when switching site domains but where data is still stored in the database with an incorrect domain.

Requires an array of domains to replace to be defined in wp-config.php:

```php
define( 'URLS_TO_REPLACE', [
	"https://google.com/"
]);
```

This will replace all instances of https://google.com in the wordpress output HTML with the current site URL.