=== Replace Old URLs ===
Contributors: Jay Williams <admin@jaywilliams.me>
Requires at least: 2.1.0
Tested up to: 5.8
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Internal package to replace URLs in output. Can be used for example when switching site domains but where data is still stored in the database with an incorrect domain.